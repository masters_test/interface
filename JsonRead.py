from json import loads
from os import getcwd

class JsonClass(object):

    def __init__(self,xml):
        self.data = []
        root_path = getcwd()
        path =(root_path + "\\data\\" + str(xml))
        with open(path) as data_file:
                self.data = loads(data_file.read())


    def getFiles(self):
        testeFile = self.data['ALL_tests']['testeFile']
        return testeFile

    def get_apps(self):
        kill_process = self.data['ALL_tests']['kill_process']
        return kill_process

if __name__=='__main__':
    xml = "testesALL.json"
    obj = JsonClass(xml)
    print obj.getFiles()