# coding=utf-8
import threading
import traceback
import sys, os
from time import *
from datetime import datetime
from subprocess import Popen, PIPE, CREATE_NEW_CONSOLE
from JsonRead import JsonClass
from kill_process import KillProcess


class Interface(object):

    """
        Mini Interface executa os testes lendo de um .Json, o teste selecionado eh executado dentro de uma thread que
         tem um time pra finalizar, o teste *.py que é executado em um subprocesso, e esse subprocesso tem um time
          que passado eh morto de forma recursiva.
    """

    def __init__(self):
        self.end = ""
        self.start = ""
        self.output = ""
        self.command = ""
        self.process = ""
        self.error = ""
        self.pid = ""
        self.kill = KillProcess()
        self.testes_name = []

    def log(self, name, string):
        nome = name
        path_root = os.getcwd()
        path = os.path.join(path_root + "\\Results\\", nome + str(datetime.now().strftime("%Y-%b-%d_%H-%M")) + ".txt")

        with open(path, 'w+') as file_log:
            file_log.write(str(string))
            file_log.close()
        print "Log Criado: " + str(path)

    def target(self):
        try:
            self.process = Popen([sys.executable, self.command], creationflags=CREATE_NEW_CONSOLE,
                                 stdin=PIPE, stdout=PIPE, stderr=PIPE, )

            self.pid = self.process.pid
            self.process.wait()
            self.error, self.output = self.process.communicate()
            self.status = self.process.returncode
            self.process.terminate()

        except Exception, e:
            self.error = traceback.format_exc()
            self.status = e
            print ("Exception in function RUN", e)

    def run(self, command, file, json_file, timeout=360):

        """ Run a command then return: (status, output, error). """
        try:
            print timeout
            self.end = mktime(localtime()) + (timeout)
            self.start = time()
            self.command = command
            print self.command

            # thread
            thread = threading.Thread(target=self.target)
            thread.start()
            thread.join(timeout+5)  # Tem 5 segundos pra finalizar de forma recursiva

            # Espera o tempo determinado acabar, senao vai iniciar varios  testes ao mesmo tempo.
            while thread.is_alive():
                if (mktime(localtime()) >= self.end):
                    print self.getDuration(self.start)
                    self.kill.kill_recursive(self.pid, json_file)  # Mata os processos de forma recursiva.
                    duration = self.getDuration(self.start)
                    self.status = "TimeOut Processo Terminado: " +str(duration)
                    break

        except:
            print "Except entrada!"
            self.log(file + "_", "Status: " + str(self.status) + "\nOutput: " + str(self.output) + "\nError: " \
                 + str(self.error))

        return self.status, self.output, self.error

    # Lista todos os arquivos no diretorio informado, e retorna um dic com os caminhos.
    def find_dir_file(self):
        root = os.getcwd()
        dic_files = []
        for path, subdirs, files in os.walk(root):
            for name in files:
                dic_files.append(os.path.join(path, name))
        return dic_files

    # Vai selecionar os testes a serem executados.
    def select_testes(self, files, json_file):
        testes_file = []
        testes_name = []
        json = JsonClass(json_file)
        dic_files = json.getFiles()
        for test in dic_files:
            for file in files:
                if test in file and test not in testes_file and "tests" in file and ".txt" not in file:
                    print file
                    testes_file.append(file)
                    self.testes_name.append(test)
        return testes_file

    # Funcao principal para executar o teste.
    def run_test(self, json_file):
        i=0
        files = self.find_dir_file()
        testes_sel = self.select_testes(files, json_file)
        print testes_sel

        for item in testes_sel:

            print datetime.now().strftime("%Y-%b-%d %H:%M:%S.%f")[:-3]
            print self.run(item, self.testes_name[i], json_file, timeout=7)
            self.log(str(self.testes_name[i]), "Status: " + str(self.status) + "\nOutput: " + str(self.output) + "\nError: " \
                 + str(self.error))
            i += 1

    def getDuration(self, start):
        end = time()
        hours, rem = divmod(end - start, 3600)
        minutes, seconds = divmod(rem, 60)
        return str("{:0>2}:{:0>2}:{:05.2f}".format(int(hours), int(minutes), seconds))

def testesJson():
    tests_json = "testesALL.json" # Por padrao o arquivo de testes eh testesALL.json
    path = os.path.join(os.getcwd(), "configTestes.txt") #
    with open(path, 'r+') as file_log:
        tests_json_txt = file_log.read().lstrip()
        if tests_json_txt != "":
            tests_json = tests_json_txt
        file_log.close()
    return tests_json

if __name__ == '__main__':
    print "The arguments are: ", str(sys.argv)
    json_file = None
    try:
        json_file = str(sys.argv[2])
    except:
        if json_file is None or json_file == "":
            print "Nao foi passado o Json de testes, definindo o padrao do configTestes.txt"
            json_file = testesJson()
    obj = Interface()
    obj.run_test(json_file)
