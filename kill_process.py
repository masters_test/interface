from psutil import Process,process_iter,NoSuchProcess
from subprocess import check_output
from JsonRead import JsonClass


class KillProcess(object):

    # Funcao do PsUtil pra matar os processos filhos de forma recursiva.
    # caso der erro ao matar o processo, no .json tem os nome dos arquivos a serem mortos.
    def kill_recursive(self, proc_pid, json_file):
        print ("Matando o Processo de Forma Recursiva: " +str(proc_pid))
        try:
            process = Process(proc_pid)
            for proc in process.children(recursive=True):
                proc.kill()
            process.kill()
        except NoSuchProcess:
            print ("o processo nao existe mais.")
            self.list_kill(json_file)

    # funcao que vai pegar os nomes dos processos a serem mortos.
    # e depois verificar se os mesmo estao em memoria.
    def list_kill(self,json_file):

        json_class = JsonClass(json_file)
        dic_apps = json_class.get_apps()
        for app in dic_apps:
            print app
            for proc in process_iter():
                try:
                    pinfo = proc.as_dict(attrs=['pid', 'name'])
                    if pinfo['name'] == app:
                        self.kill_force(app)
                except NoSuchProcess:
                    print ("o processo nao existe mais.")

    # Funcao que forca matar o processo via taskkil no console.
    def kill_force(self, app):
        if app:
            try:
                check_output('taskkill /IM ' + str(app) + ' /F', shell=True)
                return True
            except Exception, e:
                print ("Erro Desconhecido: ", e)
                return False


if __name__ =='__main__':
    obj = KillProcess()
    obj.list_kill("testesALL.json")
