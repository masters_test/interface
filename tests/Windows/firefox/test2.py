import unittest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from time import sleep
import selenium.webdriver.firefox.webdriver as fwb
from selenium.webdriver.firefox.firefox_binary import FirefoxBinary

class Google(unittest.TestCase):

    def setUp(self):
        import os
        profile = webdriver.FirefoxProfile()
        profile.set_preference("xpinstall.signatures.required", True)
        self.driver = webdriver.Firefox()

        #self.driver.set_context("xpinstall.signatures.required", False)

    def test_search_in_python_org(self):
        driver = self.driver
        driver.get("http://www.google.com")
        self.assertIn("Google", driver.title)
        elem = driver.find_element_by_name("q")
        elem.send_keys("pycon")
        elem.send_keys(Keys.RETURN)
        sleep(5)
        assert "No results found." not in driver.page_source


    def tearDown(self):
        self.driver.close()

if __name__ == "__main__":
    unittest.main()